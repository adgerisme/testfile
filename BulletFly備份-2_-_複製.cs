﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//----更新資料----
//  子彈能飛行一段距離後轉彎
//
//----功能表-----
public class BulletFly : MonoBehaviour
{
    
    Rigidbody bullet_Rigidbody;                             //宣告 子彈剛體    
    
    private Transform _targetEnemy;                         //宣告 目標敵人Transfom *由PonPon.cs控制

    bool bulletGravity;                                     //宣告 子彈重力狀態      *由PonPon.cs控制
    bool bulletTrack;                                       //宣告 子彈追蹤狀態      *由PonPon.cs控制
    public static bool bulletGo;                            //宣告 子彈追蹤狀態      *由PonPon.cs控制

    /*public static BulletFly Instance;
     
     void Awake()
     {
         Instance = this;
     }*/
  

    // Start is called before the first frame update
    void Start()
    {
        
        bullet_Rigidbody = this.GetComponent<Rigidbody>();  //初始化剛體資訊

        _targetEnemy = PonPon.scenesOfEnemy;                //初始化目標敵人Transfom   *由PonPon.cs控制

        bulletGravity = PonPon.bulletGravity;               //初始化 子彈重力狀態      *由PonPon.cs控制
        bulletTrack = PonPon.bulletTrack;                   //初始化 子彈追蹤狀態      *由PonPon.cs控制

        bulletGo = false;         
        Debug.Log(this.name+" 的剛開始 :"+bulletGo);     
        
    }

    
    void OnBecameInvisible()                                //超出攝影機範圍刪除
    {
        if (this.enabled)
        {
            //Debug.Log("過頭啦");                          //測試用訊息 出界提示
            Destroy(this.gameObject);                       //刪除字身子彈
        }
    }                               
    private void OnCollisionEnter(Collision collision)      //物裡碰撞發生時觸發
    {
        
        if(collision.gameObject.tag != "bullet"){                           //判斷碰撞目標 *碰撞體 tag是否為敵人
        //Debug.Log(this.name+" 與 "+collision.transform.name+" 擦撞喔 ");   //測試用訊息 子彈自身與誰碰撞
        //Debug.Log("命中目標 :" + collision.gameObject.name);
        CancelInvoke("_invoke");                                            //停止追蹤函式[中繼] *詳細見 _invoke 函式
        Destroy(this.gameObject);                                           //刪除自身物件
        }        
        
    }
    
    // Update is called once per frame
    void Update()
    {
        if(!bulletGo){
            return;
        }else
        {
            Debug.Log(this.name+" 的現在: "+bulletGo);  
            Invoke("_invoke", 1);                   //呼叫(追蹤函式[中繼],幾秒後) 
        }
    }    
  
    void _invoke()                                                          //呼叫追蹤方法 
    {                                                                       //ps:因為用invoke無法給予函式參數
        //Destroy(this.gameObject,5);                       //存在超過5秒刪除
        if(!bulletTrack)return;                                             //所以得多這一層 
        trackEnemy(_targetEnemy, 30);                    //呼叫追蹤函式[真](目標敵人,追蹤啟動速度)     
        Debug.Log(_targetEnemy);
    } 
    /*void _firingBullet(GameObject _bullet,Transform direction,float fireAngle,float fireSpeed)   // 射擊 (發射物體,發射方向,發射角度,發射速度)
    {
        if(!_bullet)return;                                                                  //子彈不在場上 return
        //Debug.Log("發射"+_bullet.name+"子彈");
        _bullet.transform.Rotate(0, 0, 0);                                                   // 設置角度
        _bullet.GetComponent<Rigidbody>().velocity = direction.transform.forward * fireSpeed;//給予物理力量
                    
                                                  
    }*/
    
    void trackEnemy(Transform targetEnemy,float speed)                      //追蹤 (追蹤目標,追蹤啟動速度) #僅追蹤一次
    {
        //Debug.Log(this.name+": 追蹤程序啟動");                                         //測試用訊息 追蹤啟動提示
        this.transform.LookAt(targetEnemy);                                 //子彈本身看向目標敵人
        //this.transform.Rotate(0,0,0); 
        bullet_Rigidbody.velocity = this.transform.forward * speed;         //給予物理力量        
    }
    
   
}
